---
layout: page
title: Issues
permalink: /issues/
---
* [Client](https://gitlab.com/kalilinux/nethunter/store/nethunter-store-client/issues) issue tracker: If you’re having a problem with the NetHunter Store client application,
please check if we already know about it, and/or report it in this issue tracker.

* [RFP](https://gitlab.com/kalilinux/nethunter/store/rfp/issues) tracker: To request a new app be added, submit a "Request For Packaging" on the
RFP tracker.

* [Data](https://gitlab.com/kalilinux/nethunter/store/nethunter-storedata/issues) issue tracker: For problems relating to the contents of the repository,
such as missing or outdated applications,
please use this issue tracker.

* [Website](https://gitlab.com/kalilinux/nethunter/store/nethunter-store-website/issues) issue tracker: For issues relating to this web site, you can use
this issue tracker.
